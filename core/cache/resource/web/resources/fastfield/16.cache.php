<?php  return array (
  '16.wwdAttractingTheBestTitle' => 'ATTACHING THE BEST',
  '16.wwdAttractingTheBestText' => '<p><span style="line-height: 1.5em;">We build strength through mutually beneficial relationships with our staff, clients and partners:</span></p>
<ul>
<li>
<p><span style="line-height: 1.5em;">we attract the best people globally to work with;</span></p>
</li>
<li>
<p><span style="line-height: 1.5em;">we operate at the world’s top trading venues;</span></p>
</li>
<li>
<p><span style="line-height: 1.5em;">we respect our partners with the best prices and services we can give, every time;</span></p>
</li>
<li>
<p><span style="line-height: 1.5em;">we demand the highest degree of business ethics in our corporate culture;</span></p>
</li>
<li>
<p><span style="line-height: 1.5em;">we champion the individual and value human interactions over processes and tools; we insist on partner collaboration over contract negotiation.</span></p>
</li>
</ul>
<p> <span style="line-height: 1.5em;">Corporate best practice? – We prefer <em>THE </em> <em>best practice</em>!</span></p>',
);