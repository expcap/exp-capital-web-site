<?php  return array (
  '15.wwdPushingTechnologiesTitle' => 'PUSHING TECHNOLOGY TO THE LIMIT',
  '15.wwdPushingTechnologiesText' => '<p>Technology <em>is </em>our business:</p>
<ul>
<li>proprietary trading platform, developed in-house;</li>
<li>cutting edge software development methodologies and tools;</li>
<li>heavy use of HPC &amp; advanced data visualisation;</li>
<li>some of the world\'s best scientists are involved in our research;</li>
<li>private global financial networks, spanning 30 data-centres, using a large range of technical solutions from FPGA and Java to massive parallel processing.
<p> </p>
</li>
</ul>',
);