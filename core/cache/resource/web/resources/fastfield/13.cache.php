<?php  return array (
  '13.wwdExpStationImage' => 'assets/img/exp-station-product.png',
  '13.wwdMarketMakingBtnText2' => '<p><span style="font-size: 12px; line-height: 1.5em;">Perfect pricing from beautiful mathematics.</span></p>
<p><span style="font-size: 12px; line-height: 1.5em;">Our sophisticated approach to market making enables us to provide liquidity of the highest quality.</span></p>
<p>We guarantee:</p>
<p>- Very tight spreads</p>
<p>- An incredibly low rejection ratio</p>
<p>- 24h support</p>
<p>- Unbeatable reliability</p>
<p> </p>',
);