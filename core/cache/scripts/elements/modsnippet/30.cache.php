<?php  return 'function elements_modsnippet_30($scriptProperties= array()) {
global $modx;
if (is_array($scriptProperties)) {
extract($scriptProperties, EXTR_SKIP);
}
$resource = $modx->getObject(\'modResource\', array(\'alias\' => \'people\', \'context_key\'=>$modx->context->key));

$images = $resource->getTVValue(\'about.people.images\');

$images = json_decode($images, true);

$row = $row;

$output = \'\';
foreach ($images as $image)
{
    $output .= $modx->getChunk(\'about.people.image.gallery.tpl\', array(
        \'image\' => $image[\'image\']
    ));
}

return $output;
}
';