<?php  return 'function elements_modsnippet_25($scriptProperties= array()) {
global $modx;
if (is_array($scriptProperties)) {
extract($scriptProperties, EXTR_SKIP);
}
$resource = $modx->getObject(\'modResource\', array(\'alias\' => \'integrated-risk-management\', \'context_key\' => $modx->context->key));
$data = $resource->getTVValue(\'wwd.integrated_risk_management.data\');

$data = json_decode($data, true);

$output = \'\';

for ($i = 1; $i <= count($data); $i++)
{
    $output .= $modx->getChunk(\'wwd.integrated_risk_management.btn_text.tpl\', array(
        \'id\' => $i,
        \'text\' => $data[$i - 1][\'text\']
        ));
}

return $output;
}
';