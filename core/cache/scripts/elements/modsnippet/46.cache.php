<?php  return 'function elements_modsnippet_46($scriptProperties= array()) {
global $modx;
if (is_array($scriptProperties)) {
extract($scriptProperties, EXTR_SKIP);
}
$resource = $modx->getObject(\'modResource\', array(\'alias\' => \'current-openinigs\', \'context_key\' => $modx->context->key));

$data = $resource->getTVValue(\'careers.current_openings.vacancies\');

$data = json_decode($data, true);
return sizeof($data);
}
';