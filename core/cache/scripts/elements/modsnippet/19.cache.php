<?php  return 'function elements_modsnippet_19($scriptProperties= array()) {
global $modx;
if (is_array($scriptProperties)) {
extract($scriptProperties, EXTR_SKIP);
}
$resource = $modx->getObject(\'modResource\', array(\'alias\' => \'leadership\', \'context_key\'=>$modx->context->key));
$leader = $resource->getTVValue(\'about.leadership.info\');

$leader = json_decode($leader, true);

$output = \'\';

for ($i = 0; $i < count($leader); $i++)
{
    $output .= $modx->getChunk(\'about.leader.text.tpl\', array(
        \'title\' => $leader[$i][\'name\'],
        \'text\' => $leader[$i][\'text\'],
        \'position\' => $leader[$i][\'position\'],
        \'i\' => $i
        ));
}

return $output;
}
';