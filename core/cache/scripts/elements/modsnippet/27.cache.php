<?php  return 'function elements_modsnippet_27($scriptProperties= array()) {
global $modx;
if (is_array($scriptProperties)) {
extract($scriptProperties, EXTR_SKIP);
}
$resource = $modx->getObject(\'modResource\', array(\'alias\' => \'benefits\', \'context_key\' => $modx->context->key));

$data = $resource->getTVValue(\'careers.benefits.data\');

$data = json_decode($data, true);

$id = $id;

$benefit = $data[$id - 1];

$output = \'\';

$output .= $modx->getChunk(\'careers.benefits.title.tpl\', array(
    \'image\' => $benefit[\'image\'],
    \'title\' => $benefit[\'name\'],
    \'text\' => $benefit[\'list\']
    ));
    
return $output;
}
';