<?php  return 'function elements_modsnippet_44($scriptProperties= array()) {
global $modx;
if (is_array($scriptProperties)) {
extract($scriptProperties, EXTR_SKIP);
}
$resource = $modx->getObject(\'modResource\', array(\'alias\' => \'our-culture\', \'context_key\'=>$modx->context->key));

$images = $resource->getTVValue(\'careers.our_culture.images\');

$images = json_decode($images, true);

$output = \'\';
for ($i = 1; $i <= sizeof($images); $i++)
{
    $output .= $modx->getChunk(\'careers.our_culture.image.tabs.tpl\', array(\'i\' => $i));
}

return $output;
}
';