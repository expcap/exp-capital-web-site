<?php /* Smarty version Smarty-3.0.4, created on 2014-04-19 04:59:54
         compiled from "/home/content/56/9852656/html/manager/templates/default/error.tpl" */ ?>
<?php /*%%SmartyHeaderCode:15532065715352653ab869a7-04365617%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f695d257530a0832430b511de85b78caa34816b4' => 
    array (
      0 => '/home/content/56/9852656/html/manager/templates/default/error.tpl',
      1 => 1397576983,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15532065715352653ab869a7-04365617',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="modx_error">
    <h2>Error!</h2>
    
    <p><?php echo (isset($_smarty_tpl->getVariable('_e')->value['message']) ? $_smarty_tpl->getVariable('_e')->value['message'] : null);?>
</p>
    
    <?php if (count((isset($_smarty_tpl->getVariable('_e')->value['errors']) ? $_smarty_tpl->getVariable('_e')->value['errors'] : null))>0){?>
    <p></p>
    <p><strong>Errors:</strong></p>
    <ul>
    <?php  $_smarty_tpl->tpl_vars['error'] = new Smarty_Variable;
 $_from = (isset($_smarty_tpl->getVariable('_e')->value['errors']) ? $_smarty_tpl->getVariable('_e')->value['errors'] : null); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['error']->key => $_smarty_tpl->tpl_vars['error']->value){
?>
        <li><?php echo (isset($_smarty_tpl->tpl_vars['error']->value) ? $_smarty_tpl->tpl_vars['error']->value : null);?>
</li>
    <?php }} ?>
    </ul>
    <?php }?>
</div>