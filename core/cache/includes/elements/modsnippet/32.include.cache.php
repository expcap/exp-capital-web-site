<?php
function elements_modsnippet_32($scriptProperties= array()) {
global $modx;
if (is_array($scriptProperties)) {
extract($scriptProperties, EXTR_SKIP);
}
$resource = $modx->getObject('modResource',array('alias'=> 'current-openinigs','context_key'=>$modx->context->key));

$vacancies = $resource->getTVValue('careers.current_openings.vacancies');

$vacancies = json_decode($vacancies, true);

$output = '';

for ($i = 1; $i <= sizeof($vacancies); $i++)
{
    $output .= $modx->getChunk('careers.vacancy.list.tpl', array(
        'name' => $vacancies[$i - 1]['name'],
        'city' => $vacancies[$i - 1]['city'],
        'i' => $i
        ));
}

return $output;
}
