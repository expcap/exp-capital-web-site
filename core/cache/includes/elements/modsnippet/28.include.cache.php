<?php
function elements_modsnippet_28($scriptProperties= array()) {
global $modx;
if (is_array($scriptProperties)) {
extract($scriptProperties, EXTR_SKIP);
}
$list = json_decode($list, true);

$output = '';

foreach ($list as $list_cur)
{
    $output .= $modx->getChunk('careers.benefits.list.tpl', array(
        'text' => $list_cur['text']
        ));
}

return $output;
}
