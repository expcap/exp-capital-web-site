<?php
function elements_modsnippet_18($scriptProperties= array()) {
global $modx;
if (is_array($scriptProperties)) {
extract($scriptProperties, EXTR_SKIP);
}
$resource = $modx->getObject('modResource', array('alias' => 'leadership', 'context_key'=>$modx->context->key));
$leader = $resource->getTVValue('about.leadership.info');

$leader = json_decode($leader, true);

$output = '';

for ($i = 0; $i < count($leader); $i++)
{
    $output .= $modx->getChunk('about.leader.btn.tpl', array('btn' => $leader[$i]['btn'], 'i' => $i));
}

return $output;
}
