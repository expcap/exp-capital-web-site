<?php
function elements_modsnippet_50($scriptProperties= array()) {
global $modx;
if (is_array($scriptProperties)) {
extract($scriptProperties, EXTR_SKIP);
}
$resource = $modx->getObject('modResource', array('alias' => 'grow-with-us', 'context_key' => $modx->context->key));

$library = $resource->getTVValue('careers.grow_with_us.images');

$library = json_decode($library, true);

return sizeof($library);
}
