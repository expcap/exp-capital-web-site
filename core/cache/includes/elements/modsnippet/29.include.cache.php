<?php
function elements_modsnippet_29($scriptProperties= array()) {
global $modx;
if (is_array($scriptProperties)) {
extract($scriptProperties, EXTR_SKIP);
}
$resource = $modx->getObject('modResource', array('alias' => 'people', 'context_key'=>$modx->context->key));

$images = $resource->getTVValue('about.people.images');

$images = json_decode($images, true);

$row = $row;

if ($row == 1)
{
    $start = 1;
    $end = 5;
}
if ($row == 2)
{
    $start = 6;
    $end = 9;
}
$output = '';
for ($i = $start; $i <= $end; $i++)
{
    $output .= $modx->getChunk('about.people.image.tpl', array(
        'image' => $images[$i - 1]['image'],
        'i' => $i - 1
        ));
}

return $output;
}
