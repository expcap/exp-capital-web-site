<?php
function elements_modsnippet_16($scriptProperties= array()) {
global $modx;
if (is_array($scriptProperties)) {
extract($scriptProperties, EXTR_SKIP);
}
$resource = $modx->getObject('modResource', array('alias' => 'essential-facts', 'context_key' => $modx->context->key));
$stats = $resource->getTVValue('about.essential_facts.column.3');

$stats = json_decode($stats, true);

$output = '';

$count = count($stats);
$col_ceil = ceil($count / 2);


for ($i = 1; $i <= $count; $i++)
{
    $col_start = 0; //начало колонки
    $col_end = 0;   //конец колонки

    if (($i == 1) || ($i == $col_ceil + 1))
        $col_start = 1;
    if (($i == $col_ceil) || ($i == $count))
        $col_end = 1;
    $output .= $modx->getChunk('about.essential.facts.stats.tpl', array(
        'col_start' => $col_start,
        'col_end' => $col_end,
        'comment' => $stats[$i - 1]['commentary'],
        'number' => $stats[$i - 1]['number']
        ));
}

return $output;
}
