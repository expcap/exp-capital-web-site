<?php
function elements_modsnippet_20($scriptProperties= array()) {
global $modx;
if (is_array($scriptProperties)) {
extract($scriptProperties, EXTR_SKIP);
}
$resource = $modx->getObject('modResource', array('alias' => 'partners', 'context_key'=>$modx->context->key));
$photos = $resource->getTVValue('about.partners.photos.1');

$photos = json_decode($photos, true);

$output = '';

for ($i = 0; $i < count($photos); $i++)
{
    $output .= $modx->getChunk('about.partners.photo.tpl', array('photo' => $photos[$i]['image']));
}

return $output;
}
