<?php
function elements_modsnippet_48($scriptProperties= array()) {
global $modx;
if (is_array($scriptProperties)) {
extract($scriptProperties, EXTR_SKIP);
}
$resource = $modx->getObject('modResource', array('alias' => 'grow-with-us', 'context_key' => $modx->context->key));

$library = $resource->getTVValue('careers.grow_with_us.images');

$library = json_decode($library, true);

$output = '';
for ($i = 1; $i <= sizeof($library); $i++)
{
    $output .= $modx->getChunk('careers.grow_with_us.image.gallery.tpl', array(
        'image' => $library[$i - 1]['image'],
        'caption' => $library[$i - 1]['caption'],
        'author' => $library[$i - 1]['author'],
        'info' => $library[$i - 1]['info'],
        'desc' => $library[$i - 1]['description'],
        'ref' => $library[$i - 1]['ref'],
        'i' => $i
    ));
}

return $output;
}
