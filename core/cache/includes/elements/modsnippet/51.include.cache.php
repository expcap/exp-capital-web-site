<?php
function elements_modsnippet_51($scriptProperties= array()) {
global $modx;
if (is_array($scriptProperties)) {
extract($scriptProperties, EXTR_SKIP);
}
$resource = $modx->getObject('modResource', array('alias' => 'people', 'context_key'=>$modx->context->key));

$images = $resource->getTVValue('about.people.images');

$images = json_decode($images, true);

$output = '';

for ($i = 1; $i <= sizeof($images); $i++)
{
    $output .= $modx->getChunk('about.people.gallery.tabs');
}

return $output;
}
