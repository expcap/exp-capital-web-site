<?php return array (
  '441f1b2eafb4e9cc5693f60b72d84d53' => 
  array (
    'criteria' => 
    array (
      'name' => 'ace',
    ),
    'object' => 
    array (
      'name' => 'ace',
      'path' => '{core_path}components/ace/',
      'assets_path' => '',
    ),
  ),
  '5d20e942ce37e45d6649d51be60691d1' => 
  array (
    'criteria' => 
    array (
      'name' => 'Ace',
    ),
    'object' => 
    array (
      'id' => 1,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'Ace',
      'description' => 'Ace code editor plugin for MODx Revolution',
      'editor_type' => 0,
      'category' => 0,
      'cache_type' => 0,
      'plugincode' => '/**
 * Ace Source Editor Plugin
 *
 * Events: OnManagerPageBeforeRender, OnRichTextEditorRegister, OnSnipFormPrerender,
 * OnTempFormPrerender, OnChunkFormPrerender, OnPluginFormPrerender,
 * OnFileCreateFormPrerender, OnFileEditFormPrerender, OnDocFormPrerender
 *
 * @author Danil Kostin <danya.postfactum(at)gmail.com>
 *
 * @package ace
 */
if ($modx->event->name == \'OnRichTextEditorRegister\') {
    $modx->event->output(\'Ace\');
    return;
}

if ($modx->getOption(\'which_element_editor\',null,\'Ace\') !== \'Ace\') {
    return;
}

$ace = $modx->getService(\'ace\',\'Ace\',$modx->getOption(\'ace.core_path\',null,$modx->getOption(\'core_path\').\'components/ace/\').\'model/ace/\');

$ace->initialize();

if ($modx->event->name == \'OnManagerPageBeforeRender\') {
    // older modx versions has no these styles
    $modx->controller->addHtml(\'<style>\'."
        .x-form-textarea{
        border-radius:2px;
        position: relative;
        background-color: #fbfbfb;
        background-image: none;
        border: 1px solid;
        border-color: #CCCCCC;
        }
        .x-form-focus {
        border-color: #658F1A;
        background-color: #FFFFFF;
        }
    ".\'</style>\');
    return;
}

$extensionMap = array(
    \'tpl\'   => \'text/html\',
    \'htm\'   => \'text/html\',
    \'html\'  => \'text/html\',
    \'css\'   => \'text/css\',
    \'scss\'  => \'text/x-scss\',
    \'less\'  => \'text/x-less\',
    \'svg\'   => \'image/svg+xml\',
    \'xml\'   => \'application/xml\',
    \'js\'    => \'application/javascript\',
    \'json\'  => \'application/json\',
    \'php\'   => \'application/x-php\',
    \'sql\'   => \'text/x-sql\',
    \'txt\'   => \'text/plain\',
);

switch ($modx->event->name) {
    case \'OnSnipFormPrerender\':
        $field = \'modx-snippet-snippet\';
        $mimeType = \'application/x-php\';
        break;
    case \'OnTempFormPrerender\':
        $field = \'modx-template-content\';
        $mimeType = \'text/html\';
        break;
    case \'OnChunkFormPrerender\':
        $field = \'modx-chunk-snippet\';
        if ($modx->controller->chunk && $modx->controller->chunk->isStatic()) {
            $extension = pathinfo($modx->controller->chunk->getSourceFile(), PATHINFO_EXTENSION);
            $mimeType = isset($extensionMap[$extension]) ? $extensionMap[$extension] : \'text/plain\';
        } else {
            $mimeType = \'text/html\';
        }
        break;
    case \'OnPluginFormPrerender\':
        $field = \'modx-plugin-plugincode\';
        $mimeType = \'application/x-php\';
        break;
    case \'OnFileCreateFormPrerender\':
        $field = \'modx-file-content\';
        $mimeType = \'text/plain\';
        break;
    case \'OnFileEditFormPrerender\':
        $field = \'modx-file-content\';
        $extension = pathinfo($scriptProperties[\'file\'], PATHINFO_EXTENSION);
        $mimeType = isset($extensionMap[$extension]) ? $extensionMap[$extension] : \'text/plain\';
        break;
    case \'OnDocFormPrerender\':
        if (!$modx->controller->resourceArray) {
            return;
        }
        $field = \'ta\';
        $mimeType = $modx->getObject(\'modContentType\', $modx->controller->resourceArray[\'content_type\'])->get(\'mime_type\');
        if ($modx->getOption(\'use_editor\')){
            $richText = $modx->controller->resourceArray[\'richtext\'];
            $classKey = $modx->controller->resourceArray[\'class_key\'];
            if ($richText || in_array($classKey, array(\'modStaticResource\',\'modSymLink\',\'modWebLink\',\'modXMLRPCResource\'))) {
                $field = false;
            }
        }

        break;
    default:
        return;
}

$script = "";

if ($field) {
    $script .="
    setTimeout(function(){
        var textArea = Ext.getCmp(\'$field\');
        var textEditor = MODx.load({
            xtype: \'modx-texteditor\',
            enableKeyEvents: true,
            anchor: textArea.anchor,
            width: \'auto\',
            height: textArea.height,
            name: textArea.name,
            value: textArea.getValue(),
            mimeType: \'$mimeType\'
        });

        textArea.el.dom.removeAttribute(\'name\');
        textArea.el.setStyle(\'display\', \'none\');
        textEditor.render(textArea.el.dom.parentNode);
        textArea.setSize = function(){textEditor.setSize.apply(textEditor, arguments)}
        textEditor.on(\'keydown\', function(e){textArea.fireEvent(\'keydown\', e);});
        MODx.load({
            xtype: \'modx-treedrop\',
            target: textEditor,
            targetEl: textEditor.el,
            onInsert: (function(s){
                this.insertAtCursor(s);
                this.focus();
                return true;
            }).bind(textEditor),
            iframe: true
        });
    });";
}

if ($modx->event->name == \'OnDocFormPrerender\' && !$modx->getOption(\'use_editor\')) {
    $script .= "
    var textAreas = Ext.query(\'.modx-richtext\');
    textAreas.forEach(function(textArea){
        var htmlEditor = MODx.load({
            xtype: \'modx-texteditor\',
            width: \'auto\',
            height: parseInt(textArea.style.height) || 200,
            name: textArea.name,
            value: textArea.value,
            mimeType: \'text/html\'
        });

        textArea.name = \'\';
        textArea.style.display = \'none\';

        htmlEditor.render(textArea.parentNode);
        htmlEditor.editor.on(\'key\', function(e){ MODx.fireResourceFormChange() });
    });";
}

$modx->controller->addHtml(\'<script>Ext.onReady(function() {\' . $script . \'});</script>\');',
      'locked' => 0,
      'properties' => NULL,
      'disabled' => 0,
      'moduleguid' => '',
      'static' => 1,
      'static_file' => 'ace/elements/plugins/ace.plugin.php',
      'content' => '/**
 * Ace Source Editor Plugin
 *
 * Events: OnManagerPageBeforeRender, OnRichTextEditorRegister, OnSnipFormPrerender,
 * OnTempFormPrerender, OnChunkFormPrerender, OnPluginFormPrerender,
 * OnFileCreateFormPrerender, OnFileEditFormPrerender, OnDocFormPrerender
 *
 * @author Danil Kostin <danya.postfactum(at)gmail.com>
 *
 * @package ace
 */
if ($modx->event->name == \'OnRichTextEditorRegister\') {
    $modx->event->output(\'Ace\');
    return;
}

if ($modx->getOption(\'which_element_editor\',null,\'Ace\') !== \'Ace\') {
    return;
}

$ace = $modx->getService(\'ace\',\'Ace\',$modx->getOption(\'ace.core_path\',null,$modx->getOption(\'core_path\').\'components/ace/\').\'model/ace/\');

$ace->initialize();

if ($modx->event->name == \'OnManagerPageBeforeRender\') {
    // older modx versions has no these styles
    $modx->controller->addHtml(\'<style>\'."
        .x-form-textarea{
        border-radius:2px;
        position: relative;
        background-color: #fbfbfb;
        background-image: none;
        border: 1px solid;
        border-color: #CCCCCC;
        }
        .x-form-focus {
        border-color: #658F1A;
        background-color: #FFFFFF;
        }
    ".\'</style>\');
    return;
}

$extensionMap = array(
    \'tpl\'   => \'text/html\',
    \'htm\'   => \'text/html\',
    \'html\'  => \'text/html\',
    \'css\'   => \'text/css\',
    \'scss\'  => \'text/x-scss\',
    \'less\'  => \'text/x-less\',
    \'svg\'   => \'image/svg+xml\',
    \'xml\'   => \'application/xml\',
    \'js\'    => \'application/javascript\',
    \'json\'  => \'application/json\',
    \'php\'   => \'application/x-php\',
    \'sql\'   => \'text/x-sql\',
    \'txt\'   => \'text/plain\',
);

switch ($modx->event->name) {
    case \'OnSnipFormPrerender\':
        $field = \'modx-snippet-snippet\';
        $mimeType = \'application/x-php\';
        break;
    case \'OnTempFormPrerender\':
        $field = \'modx-template-content\';
        $mimeType = \'text/html\';
        break;
    case \'OnChunkFormPrerender\':
        $field = \'modx-chunk-snippet\';
        if ($modx->controller->chunk && $modx->controller->chunk->isStatic()) {
            $extension = pathinfo($modx->controller->chunk->getSourceFile(), PATHINFO_EXTENSION);
            $mimeType = isset($extensionMap[$extension]) ? $extensionMap[$extension] : \'text/plain\';
        } else {
            $mimeType = \'text/html\';
        }
        break;
    case \'OnPluginFormPrerender\':
        $field = \'modx-plugin-plugincode\';
        $mimeType = \'application/x-php\';
        break;
    case \'OnFileCreateFormPrerender\':
        $field = \'modx-file-content\';
        $mimeType = \'text/plain\';
        break;
    case \'OnFileEditFormPrerender\':
        $field = \'modx-file-content\';
        $extension = pathinfo($scriptProperties[\'file\'], PATHINFO_EXTENSION);
        $mimeType = isset($extensionMap[$extension]) ? $extensionMap[$extension] : \'text/plain\';
        break;
    case \'OnDocFormPrerender\':
        if (!$modx->controller->resourceArray) {
            return;
        }
        $field = \'ta\';
        $mimeType = $modx->getObject(\'modContentType\', $modx->controller->resourceArray[\'content_type\'])->get(\'mime_type\');
        if ($modx->getOption(\'use_editor\')){
            $richText = $modx->controller->resourceArray[\'richtext\'];
            $classKey = $modx->controller->resourceArray[\'class_key\'];
            if ($richText || in_array($classKey, array(\'modStaticResource\',\'modSymLink\',\'modWebLink\',\'modXMLRPCResource\'))) {
                $field = false;
            }
        }

        break;
    default:
        return;
}

$script = "";

if ($field) {
    $script .="
    setTimeout(function(){
        var textArea = Ext.getCmp(\'$field\');
        var textEditor = MODx.load({
            xtype: \'modx-texteditor\',
            enableKeyEvents: true,
            anchor: textArea.anchor,
            width: \'auto\',
            height: textArea.height,
            name: textArea.name,
            value: textArea.getValue(),
            mimeType: \'$mimeType\'
        });

        textArea.el.dom.removeAttribute(\'name\');
        textArea.el.setStyle(\'display\', \'none\');
        textEditor.render(textArea.el.dom.parentNode);
        textArea.setSize = function(){textEditor.setSize.apply(textEditor, arguments)}
        textEditor.on(\'keydown\', function(e){textArea.fireEvent(\'keydown\', e);});
        MODx.load({
            xtype: \'modx-treedrop\',
            target: textEditor,
            targetEl: textEditor.el,
            onInsert: (function(s){
                this.insertAtCursor(s);
                this.focus();
                return true;
            }).bind(textEditor),
            iframe: true
        });
    });";
}

if ($modx->event->name == \'OnDocFormPrerender\' && !$modx->getOption(\'use_editor\')) {
    $script .= "
    var textAreas = Ext.query(\'.modx-richtext\');
    textAreas.forEach(function(textArea){
        var htmlEditor = MODx.load({
            xtype: \'modx-texteditor\',
            width: \'auto\',
            height: parseInt(textArea.style.height) || 200,
            name: textArea.name,
            value: textArea.value,
            mimeType: \'text/html\'
        });

        textArea.name = \'\';
        textArea.style.display = \'none\';

        htmlEditor.render(textArea.parentNode);
        htmlEditor.editor.on(\'key\', function(e){ MODx.fireResourceFormChange() });
    });";
}

$modx->controller->addHtml(\'<script>Ext.onReady(function() {\' . $script . \'});</script>\');',
    ),
    'files' => 
    array (
      0 => '/home/content/56/9852656/html/core/components',
    ),
  ),
  'ffa6791da8b819807240146ad0d009e4' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 1,
      'event' => 'OnChunkFormPrerender',
    ),
    'object' => 
    array (
      'pluginid' => 1,
      'event' => 'OnChunkFormPrerender',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  'b40c796cf21c99751ef07d6c5adc6159' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 1,
      'event' => 'OnPluginFormPrerender',
    ),
    'object' => 
    array (
      'pluginid' => 1,
      'event' => 'OnPluginFormPrerender',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  '3f8ad0723c6315fe65e7e8b104b55e11' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 1,
      'event' => 'OnSnipFormPrerender',
    ),
    'object' => 
    array (
      'pluginid' => 1,
      'event' => 'OnSnipFormPrerender',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  '25cf832e1cd62df6febcd28ea1fd5ccd' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 1,
      'event' => 'OnTempFormPrerender',
    ),
    'object' => 
    array (
      'pluginid' => 1,
      'event' => 'OnTempFormPrerender',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  '310a9204d1fdbf61ce0160c5713c1653' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 1,
      'event' => 'OnFileEditFormPrerender',
    ),
    'object' => 
    array (
      'pluginid' => 1,
      'event' => 'OnFileEditFormPrerender',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  'cea8d5cfd61539c8eb599774dd056023' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 1,
      'event' => 'OnFileCreateFormPrerender',
    ),
    'object' => 
    array (
      'pluginid' => 1,
      'event' => 'OnFileCreateFormPrerender',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  'a726e99a617433ed913b382280b4a99f' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 1,
      'event' => 'OnDocFormPrerender',
    ),
    'object' => 
    array (
      'pluginid' => 1,
      'event' => 'OnDocFormPrerender',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  '4459e034b64487f3868acbc02029c153' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 1,
      'event' => 'OnRichTextEditorRegister',
    ),
    'object' => 
    array (
      'pluginid' => 1,
      'event' => 'OnRichTextEditorRegister',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  '4425ebe64613e778836a8ece920ad23c' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 1,
      'event' => 'OnManagerPageBeforeRender',
    ),
    'object' => 
    array (
      'pluginid' => 1,
      'event' => 'OnManagerPageBeforeRender',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  'fba17d09fcfb6a66cfcd3bc15855dac3' => 
  array (
    'criteria' => 
    array (
      'key' => 'ace.theme',
    ),
    'object' => 
    array (
      'key' => 'ace.theme',
      'value' => 'chrome',
      'xtype' => 'textfield',
      'namespace' => 'ace',
      'area' => 'general',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  'c036fbf98e7333849e822165968e7079' => 
  array (
    'criteria' => 
    array (
      'key' => 'ace.font_size',
    ),
    'object' => 
    array (
      'key' => 'ace.font_size',
      'value' => '13px',
      'xtype' => 'textfield',
      'namespace' => 'ace',
      'area' => 'general',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  'e92fe900f2c37292585bc0281bb7907e' => 
  array (
    'criteria' => 
    array (
      'key' => 'ace.word_wrap',
    ),
    'object' => 
    array (
      'key' => 'ace.word_wrap',
      'value' => '',
      'xtype' => 'combo-boolean',
      'namespace' => 'ace',
      'area' => 'general',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  'bcdb39d1b41b9a922a8de26e219e9d8c' => 
  array (
    'criteria' => 
    array (
      'key' => 'ace.soft_tabs',
    ),
    'object' => 
    array (
      'key' => 'ace.soft_tabs',
      'value' => '1',
      'xtype' => 'combo-boolean',
      'namespace' => 'ace',
      'area' => 'general',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '397c5e2daa93d0d943e13da75565d1b4' => 
  array (
    'criteria' => 
    array (
      'key' => 'ace.tab_size',
    ),
    'object' => 
    array (
      'key' => 'ace.tab_size',
      'value' => '4',
      'xtype' => 'textfield',
      'namespace' => 'ace',
      'area' => 'general',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '4e7d4640387898dcc3c902eb0f1046dd' => 
  array (
    'criteria' => 
    array (
      'key' => 'ace.fold_widgets',
    ),
    'object' => 
    array (
      'key' => 'ace.fold_widgets',
      'value' => '1',
      'xtype' => 'combo-boolean',
      'namespace' => 'ace',
      'area' => 'general',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '1e4448d3bdbf421231ca6b7bdd4b1775' => 
  array (
    'criteria' => 
    array (
      'key' => 'ace.show_invisibles',
    ),
    'object' => 
    array (
      'key' => 'ace.show_invisibles',
      'value' => '0',
      'xtype' => 'combo-boolean',
      'namespace' => 'ace',
      'area' => 'general',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  'babf5caf989fdb8c1522d6cc39014114' => 
  array (
    'criteria' => 
    array (
      'key' => 'ace.snippets',
    ),
    'object' => 
    array (
      'key' => 'ace.snippets',
      'value' => '',
      'xtype' => 'textarea',
      'namespace' => 'ace',
      'area' => 'general',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
);
