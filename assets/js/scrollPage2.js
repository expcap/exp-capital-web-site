var scrollController = {
        1: [scrollActions.addSidebarColor, scrollActions.showScheme, scrollActions.changeScheme],
        2: [scrollActions.addSidebarColor, scrollActions.showScheme, scrollActions.changeScheme],
        3: [scrollActions.addSidebarColor, scrollActions.showScheme, scrollActions.changeScheme],
        4: [scrollActions.addSidebarColor, scrollActions.showScheme, scrollActions.changeScheme],
        5: [scrollActions.addSidebarColor, scrollActions.showScheme, scrollActions.changeScheme]
    },
    invertScrollController = {
        0: [scrollActions.removeSidebarColor, scrollActions.hideScheme],
        1: [scrollActions.changeScheme, scrollActions.showScheme],
        2: [scrollActions.changeScheme],
        3: [scrollActions.changeScheme],
        4: [scrollActions.changeScheme],
        5: [scrollActions.changeScheme]
    };