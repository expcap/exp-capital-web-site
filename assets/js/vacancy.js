$('.btn._green._sbm').on('click', function (e) {
    if (formCheck($(this))) {
        return false;
    }
    e.preventDefault();
    var name = $(this).attr('name');
    id = name.slice(4);
    var feedbackName = $('#name' + id).val();
    var feedbackSurname = $('#surname' + id).val();
    var feedbackEmail = $('#email' + id).val();
    var feedbackPhone = $('#phone' + id).val();
    var feedbackTitle = $('#title' + id).val();

    var fd = new FormData();
    fd.append('feedbackTitle', feedbackTitle);
    fd.append('feedbackName', feedbackName);
    fd.append('feedbackSurname', feedbackSurname);
    fd.append('feedbackEmail', feedbackEmail);
    fd.append('feedbackPhone', feedbackPhone);
    fd.append('files', $('#file' + id)[0].files[0]);
    //console.log(fd);
    $.ajax({
        type: "POST",
        url: "/api/feedback",
        processData: false,
        contentType: false,
        data: fd,
        dataType: 'html',
        success: function (response) {
            var form = $('form:visible')
            form.find('.success-msg').fadeIn();
            form.find('label').fadeOut();
            setTimeout(function () {
                form.find('.success-msg').fadeOut();
                form.find('label').fadeIn();
            }, 5000);
        }

    });
});

function formCheck(item) {
    var $inp = item.find('input[type=text]'),
        $ta = item.find('textarea'),
        allerts = 0;
    $inp.add($ta).each(function () {
        if ($(this).val() == "") {
            $(this).addClass('_err');
            allerts += 1;
        }
        else {
            $(this).removeClass('_err');
        }
    });
    return allerts;
}

$('.typeFile').on('change', function() {
    $(this).parent().find(".placeholder").text($(this).val());
});