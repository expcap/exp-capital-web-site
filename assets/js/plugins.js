// Avoid `console` errors in browsers that lack a console.
(function () {
    var method;
    var noop = function () {
    };
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// object.watch
if (!Object.prototype.watch) {
    Object.defineProperty(Object.prototype, "watch", {
        enumerable: false, configurable: true, writable: false, value: function (prop, handler) {
            var
                oldval = this[prop]
                , newval = oldval
                , getter = function () {
                    return newval;
                }
                , setter = function (val) {
                    oldval = newval;
                    return newval = handler.call(this, prop, oldval, val);
                }
                ;

            if (delete this[prop]) { // can't watch constants
                Object.defineProperty(this, prop, {
                    get: getter, set: setter, enumerable: true, configurable: true
                });
            }
        }
    });
}

// object.unwatch
if (!Object.prototype.unwatch) {
    Object.defineProperty(Object.prototype, "unwatch", {
        enumerable: false, configurable: true, writable: false, value: function (prop) {
            var val = this[prop];
            delete this[prop]; // remove accessors
            this[prop] = val;
        }
    });
}


//<!--
(function ($) {
    $.fn.contScroll = function (options, list) {
        var that = this,
            settings = $.extend({
                'scroll': true,
                'animate': true
            }, options),
            isScrollAct = false,
            $slider;

        this.each(function () {
            var visible = $(this).is(':visible');
            if (!visible) {
                $(this).show();
            }
            checkScrollNeed($(this))
        });

        $(window).resize(function () {
            that.each(function () {
                var visible = $(this).is(':visible');
                if (!visible) {
                    $(this).show();
                }
                checkScrollNeed($(this))
            });
        });

        function checkScrollNeed(item) {
            var par;
            if (item.parents('.screen').length) {
                par = item.parents('.screen');
            }
            else {
                par = item.parents('.pop-cont-inn');
            }

            var parOffset = par.offset().top,

                visibleHeight = par.height() - (item.offset().top - parOffset) ,
                fullHeight = item.height(),
                $wrp,
                $scroll;
            if (fullHeight > visibleHeight && !item.closest('.scrollWrp').length) {
                item.addClass('scrollable');
                item.wrap('<div class="scrollWrp" style="height:' + (visibleHeight) + 'px"></div>');
                $wrp = item.parent();
                $scroll = $('<div class="scrollBox"><div class="scrollSlider"></div></div>');
                $scroll.appendTo($wrp);
            }
            else if (visibleHeight >= fullHeight) {
                item.filter('.scrollable').unwrap();
                item.filter('.scrollable').next('.scrollBox').remove();
                item.removeClass('scrollable');
            }
        }

        $(document).on('mousedown', '.scrollSlider', function (e) {
            var point = e.pageY - $(this).offset().top;
            isScrollAct = true;
            slider($(this), point);
            return false;
        });
        $(document).on('mouseup', function () {
            $(document).off('mousemove');
            isScrollAct = false;
        });

        $('.screen, .pop-cont').on('DOMMouseScroll mousewheel', '.scrollWrp', function (e) {
            var item = $(this).find('.scrollSlider'),
                deltaX,
                $scroll = item.parent(),
                $cont = item.parents('.scrollWrp').find('.scrollable'),
                delta = e.originalEvent.detail || -e.originalEvent.wheelDelta;

            if (delta > 0) {
                deltaX = 50
            }
            else {
                deltaX = -50
            }

            delta = parseInt(item.position().top) + deltaX / 2;
            if (delta <= 0) {
                delta = 0;
            }
            if (delta >= $scroll.height() - item.height()) {
                delta = $scroll.height() - item.height();
            }
            item.css({
                top: delta
            });
            $cont.css({
                top: -item.position().top * (($cont.height() - $scroll.height()) / ($scroll.height() - item.height()))
            });
            return false;
        });

        function slider(item, point) {
            $(document).on('mousemove', function (e) {
                if (isScrollAct) {
                    var $scroll = item.parent(),
                        $cont = item.parents('.scrollWrp').find('.scrollable'),
                        delta = e.pageY - $scroll.offset().top;
                    if (delta <= point) {
                        delta = point;
                    }
                    if (delta >= $scroll.height() - item.height() + point) {
                        delta = $scroll.height() - item.height() + point;
                    }
                    item.css({
                        top: delta - point
                    });
                    $cont.css({
                        top: -item.position().top * (($cont.height() - $scroll.height()) / ($scroll.height() - item.height()))
                    });
                }
            });
        }

        /*$('.' + parClass + '-wrp').on('click', '.' + itemClass, function () {
         var text = $(this).text();
         $(this).parents('.' + parClass + '-wrp').find('.' + parClass).text(text);
         hideList();
         });*/
        return this.each(function () {
        });
    };
})(jQuery);


//<!--
(function ($) {
    $.fn.validate = function (options, list) {
        var that = this,
            settings = $.extend({
                'scroll': true,
                'animate': true
            }, options);

        this.each(function () {
            var inp = $(this).find('input'),
                txa = $(this).find('textarea'),
                items = inp.add(txa);
            checkItems(items);
        });

        function checkItems(items) {

        }

        $(document).on('mousedown', '.scrollSlider', function (e) {
            var point = e.pageY - $(this).offset().top;
            isScrollAct = true;
            slider($(this), point);
            return false;
        });

        return this.each(function () {
        });
    };
})(jQuery);


(function ($) {
    $.fn.popUp = function (options, $btn) {
        var that = this,
            settings = $.extend({
                'background': true,
                'animation': true,
                'closeBtn': true
            }, options),
            parClass = this.attr('class'),
            wrpClass = parClass + '-wrp', closeClass = parClass + '__close',
            $closeBtn = $('<div class="' + closeClass + '"></div>');

        $('.' + parClass).wrap('<div class="' + wrpClass + '"></div>');
        $('.' + wrpClass).css({
            display: 'none',
            position: 'fixed',
            width: '100%',
            height: '100%',
            top: 0,
            left: 0
        });
        this.css({
            display: 'none',
            position: 'absolute',
            top: 0,
            left: '50%',
            marginLeft: -this.outerWidth(true) / 2,
            marginTop: 100
        });

        $btn.on('click', function () {
            showPopup();
        });

        function showPopup() {
            if (settings.background) {
                if (settings.animation) {
                    $('.' + wrpClass).fadeIn();
                }
                else {
                    $('.' + wrpClass).show();
                }
            }
            if (settings.animation) {
                that.fadeIn();
            }
            else {
                that.show();
            }
        }

        function closePopup() {
            if (settings.background) {
                if (settings.animation) {
                    $('.' + wrpClass).fadeOut();
                }
                else {
                    $('.' + wrpClass).hide();
                }
            }
            if (settings.animation) {
                that.fadeOut();
            }
            else {
                that.hide();
            }
        }

        if (settings.closeBtn) {
            $closeBtn.appendTo(this).css({
                position: 'absolute'
            });
        }

        $closeBtn.on('click', function () {
            closePopup();
        });

        $(document).on('click', function (e) {
            if ($(e.target).closest(that).length || $(e.target).closest($btn).length) {
                return false;
            }
            closePopup();
            e.stopPropagation();
        });

        return this.each(function () {
        });
    };
})(jQuery);


$('.dd__steps__pup_1').popUp({
    background: true,
    animation: true,
    closeBtn: true
}, $('.dd__steps__i_1'));

$('.dd__steps__pup_2').popUp({
    background: true,
    animation: true,
    closeBtn: true
}, $('.dd__steps__i_2'));

$('.dd__steps__pup_3').popUp({
    background: true,
    animation: true,
    closeBtn: true
}, $('.dd__steps__i_3'));

$('.dd__steps__pup_4').popUp({
    background: true,
    animation: true,
    closeBtn: true
}, $('.dd__steps__i_4'));



