var isAnimated = false, //is page scrolling right now
    model, view;


model = {
    navigation: {
        pageNum: 0,
        isTouch: false,
        length: $('.screen').length,
        check: function (oldpos, curpos) {
            var invert = oldpos > curpos;
            if (!invert) {
                for (var i in scrollController) {
                    var k = parseInt(i);
                    if (curpos >= k && curpos < k + 1) {
                        for (var j = 0; j < scrollController[k].length; j += 1) {
                            scrollController[k][j](k);
                        }
                        break
                    }
                }
            }
            else {
                for (i in invertScrollController) {
                    var f = parseInt(i);
                    if (curpos <= f && curpos > f - 1) {
                        for (var l = 0; l < invertScrollController[f].length; l += 1) {
                            invertScrollController[f][l](f);
                        }
                        break
                    }
                }
            }
        }
    },
    gallery: {
        curPic: 1,
        leng: $('.gal-pop-cont').eq(0).find('.gal-i').length
    },
    index: {
        slideNum: 0,
        slideLength: $('.slide').length,
        slidesDir: 1,
        slidesTimer: null
    }
};

view = {
    navigation: {
        scroll: function (ind) {
            $('.screen').css({
                transform: 'translate3d(0, -' + ind * 100 + '%, 0)'
            });
        },
        setNavState: function (ind) {
            var navs = $('.sideScreenNav').add('.subNav'),
                sideItem = $('.sideScreenNav-i').eq(ind),
                items = $('.subNav-i').eq(ind).add(sideItem);

            navs.find('._act').removeClass('_act');
            items.addClass('_act');
        }
    },
    popup: {
        show: function (item) {
            var popup = $('.popup').add('#' + item);
            popup.off('transitionend');
            popup.css({
                visibility: 'visible',
                display: 'block',
                opacity: 1
            });
            $('.scrollCont').contScroll();
        },
        hide: function (item) {
            item.css({
                opacity: 0
            });
            view.gallery.showbg();
            item.on('transitionend', function () {
                $(this).css({
                    visibility: 'hidden',
                    display: 'none'
                })
            });
        }
    },
    booksGallery: {
        model: {
            length: $('.librery-item').length,
            item_cur: 0
        },
        events: function () {
            var that = this;
            $('.gal-arrs-i._librery._left').on('click', function () {
                if (that.model.item_cur > 0) {
                    that.model.item_cur -= 1;
                }
            });
            $('.gal-arrs-i._librery._right').on('click', function () {
                if (that.model.item_cur < that.model.length - 1) {
                    that.model.item_cur += 1;
                }
            });
        },
        hide_current: function (ind) {
            var $item = $('.librery-item'),
                $item_cur = $item.eq(ind)
            $item.off('transitionend');
            $item_cur.addClass('_trs')
                .css({
                    opacity: 0
                });
            $item_cur.on('transitionend', function () {
                $(this).css({
                    visibility: 'hidden'
                }).removeClass('_trs');
            })
        },
        show_next: function (ind) {
            var $item = $('.librery-item');
            $item.off('transitionend');
            $item.eq(ind).addClass('_trs')
                .css({
                    opacity: 1,
                    visibility: 'visible'
                })
            $item.on('transitionend', function () {
                $(this).removeClass('_trs');
            });
        },
        counter: function (ind) {
            var cur = $('.librery-counter-cur');
            cur.text(ind);
        },
        watchers: function () {
            var that = this;
            this.model.watch('item_cur', function (id, oldval, newval) {
                that.hide_current(oldval);
                that.show_next(newval);
                that.counter(newval + 1);
                return newval;
            })
        },
        init: function () {
            this.events();
            this.watchers();
        }
    },
    gallery: {
        show: function (ind, gal) {
            var slides = gal.find('.gal-i');
            slides.css({
                opacity: 0,
                zIndex: 1
            });
            $('.gal-pop-cont').css({
                opacity: 1
            });
            slides.eq(ind).addClass('_act').removeClass('_trs').css({
                opacity: 1,
                zIndex: 3
            });
            this.paging(ind, gal);
            model.gallery.curPic = ind;
        },
        hidebg: function () {
            $('.screen, .head, .sideScreenNav').css({
                opacity: 1
            });
        },
        showbg: function () {
            $('.screen, .head, .sideScreenNav').css({
                opacity: 1
            });
        },
        paging: function (ind, gal) {
            var paging = gal.find('.gal-pag-i');
            paging.removeClass('_act');
            paging.eq(ind).addClass('_act');
        },
        rotate: function (ind, gal) {
            var act = gal.find('.gal-i._act'),
                slides = gal.find('.gal-i'),
                curSlide = slides.eq(ind);
            slides.off('transitionend');
            slides.not('._act').css({
                opacity: 0,
                zIndex: 1
            });
            act.addClass('_trs').css({
                opacity: 0,
                zIndex: 3
            });

            curSlide.css({
                opacity: 1
            });

            act.on('transitionend', function () {
                $(this).removeClass('_act').removeClass('_trs');
                slides.not('._act').css({
                    zIndex: 1
                });
                curSlide.addClass('_act').css({
                    zIndex: 3
                });
            });
        }
    },
    slides: {
        change: function (newval, target) {
            var val = newval * -100;
            target.css({
                transform: 'translate3d(' + val + '%,0,0)'
            })
        },
        centrCont: function (val) {
            $('.slide').eq(val).find('.slideCont').css({
                transform: 'translate3d(' + 0 + ', 0, 0)',
                opacity: 1
            })
        },
        shiftCont: function (val) {
            $('.slide').eq(val).find('.slideCont').css({
                transform: 'translate3d(' + 50 + '%, 0, 0)',
                opacity: 0
            })
        },
        checkPos: function (newval, target, length) {
            target.removeClass('_act');
            target.eq(newval).addClass('_act');
            if (newval == length - 1) {
                $('.slideControls-arr').css({
                    opacity: 1,
                    cursor: 'pointer'
                });
                $('.slideControls-arr._right').css({
                    opacity: 0,
                    cursor: 'default'
                });
            }
            else if (newval == 0) {
                $('.slideControls-arr').css({
                    opacity: 1,
                    cursor: 'pointer'
                });
                $('.slideControls-arr._left').css({
                    opacity: 0,
                    cursor: 'default'
                });
            }
            else {
                $('.slideControls-arr').css({
                    opacity: 1,
                    cursor: 'pointer'
                });
            }
        }
    }
};

view.booksGallery.init();

function controller() {

    $(function(){
        var hash = window.location.hash.slice(1);
        model.navigation.pageNum = hash;
    });

    $('.screen').on('DOMMouseScroll mousewheel', function (e) {
        if ($(e.target).closest('.scrollWrp').length) {
            return false;
        }
        e.preventDefault();
        var delta = e.originalEvent.detail || -e.originalEvent.wheelDelta,
            dir;
        dir = (delta > 0) ? 1 : -1;
        var pageNum = model.navigation.pageNum + dir;
        if (pageNum >= 0 && pageNum < model.navigation.length && !isAnimated) {
            model.navigation.pageNum += dir;
        }
        clearTimeout($.data(this, 'timer'));
        $.data(this, 'timer', setTimeout(function () {
            isAnimated = false;
        }, 250));
    });


    if (!$('.page._index').length) {
        $(window).on('keydown', function (e) {
            var dir,
                keyup = e.keyCode == 38,
                keydown = e.keyCode == 40;

            if (keyup || keydown) {
                if (keyup) {
                    dir = -1;
                } else if (keydown) {
                    dir = 1;
                }

                var pageNum = model.navigation.pageNum + dir;
                if (pageNum >= 0 && pageNum < model.navigation.length) {
                    model.navigation.pageNum += dir;
                }

                e.preventDefault();
                return false;
            }
        });
    }

    $(window).on('resize', function () {
    });

    $(document).on('click', '.sideScreenNav-i, .subNav-i', function () {
        model.navigation.pageNum = $(this).index();
        return false;
    });

    $('.tabs-i').on('click', function () {
        var target = $('.' + $(this).parent().data('target')),
            index = $(this).index(),
            tabs = $(this).parent().find('.tabs-i');
        tabs.removeClass('_act');
        $(this).addClass('_act');
        target.removeClass('_act');
        target.each(function () {
            if ($(this).index() == index) {
                $(this).addClass('_act')
            }
        });
        $('.scrollCont').contScroll();
    });

    $('.popup').on('click', function () {
        view.popup.hide($(this));
        view.popup.hide($('.pop-cont'));
    });

    $('.pop-close').on('click', function () {
        var item = $(this).parents('.pop-cont');
        view.popup.hide($('.popup'));
        item.css({
            opacity: 0
        });
        item.on('transitionend', function () {
            $(this).css({
                display: 'none'
            })
        });
    });

    $('.popupLn').on('click', function () {
        var target = $(this).data('target');
        view.popup.show(target);
    });

    $('.galPrevs-i').on('click', function () {
        var gal = $('#' + $(this).data('target'));
        view.gallery.show($(this).data('number'), gal);
        view.gallery.hidebg();
    });

    $('.gal-arrs-i._left').on('click', function () {
        var newval = model.gallery.curPic - 1;
        if (newval < 0) {
            model.gallery.curPic = $(this).parents('.gal-pop-cont').find('.gal-i').length - 1
        }
        else {
            model.gallery.curPic -= 1;
        }
    });

    $('.gal-arrs-i._right').on('click', function () {
        var newval = model.gallery.curPic + 1;
        if (newval > $(this).parents('.gal-pop-cont').find('.gal-i').length - 1) {
            model.gallery.curPic = 0
        }
        else {
            model.gallery.curPic += 1;
        }
    });

    $('.gal-pag-i').on('click', function () {
        model.gallery.curPic = $(this).index();
    });

    $('.screenLn').on('click', function () {
        model.navigation.pageNum = $(this).data('scrnum');
    });

    $('.slideControls-arr._right').on('click', function () {
        var num = model.index.slideNum,
            length = model.index.slideLength - 1;
        if (num < length) {
            model.index.slideNum += 1;
        }
    });

    if ($('.page._index').length) {
        $(window).on('keydown', function (e) {
            var key = e.keyCode;
            if (key === 39) {
                var num = model.index.slideNum,
                    length = model.index.slideLength - 1;
                if (num < length) {
                    model.index.slideNum += 1;
                }
            }
            else if (key === 37) {
                var num = model.index.slideNum;
                if (num > 0) {
                    model.index.slideNum -= 1;
                }
            }
        })
    }


    $('.slideControls-arr._left').on('click', function () {
        var num = model.index.slideNum;
        if (num > 0) {
            model.index.slideNum -= 1;
        }
    });


    $('.indexSlideDote').on('click', function () {
        model.index.slideNum = $(this).index();
    });
    function autoSlide(dir) {
        model.index.slideNum += dir;
    }

    function startIndexInterval() {
        model.index.slidesTimer = setInterval(function () {
                var num = model.index.slideNum,
                    length = model.index.slideLength - 1;
                if (num == length) {
                    model.index.slidesDir = -1;
                } else if (num == 0) {
                    model.index.slidesDir = 1;
                }
                autoSlide(model.index.slidesDir);
            }, 5000
        );
    }

    if ($('.page._index').length) {
        startIndexInterval();
    }

    $('form').on('submit', function () {
    });

    $('.text-inp').on('focus', function () {
        $(this).removeClass('_err');
    });


//watchers
    model.navigation.watch('pageNum', function (id, oldval, newval) {
        //console.log('asdasd');
        isAnimated = true;
        model.navigation.check(oldval, newval);
        view.navigation.scroll(newval);
        view.navigation.setNavState(newval);
        window.location.hash = newval;
        return newval;
    });

    model.index.watch('slideNum', function (id, oldval, newval) {
        clearInterval(model.index.slidesTimer);
        view.slides.change(newval, $('.screen'));
        view.slides.centrCont(newval);
        view.slides.shiftCont(oldval);
        view.slides.checkPos(newval, $('.indexSlideDote'), model.index.slideLength);
        startIndexInterval();
        return newval;
    });

    model.gallery.watch('curPic', function (id, oldval, newval) {
        var gal = $('.gal-pop-cont:visible');
        view.gallery.rotate(newval, gal);
        view.gallery.paging(newval, gal);
        return newval;
    });
}

controller();

$(function () {
    $('.scrollCont').contScroll();
});
