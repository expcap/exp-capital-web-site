var schemeTimer;
var scrollActions = {
    addSidebarColor: function (i) {
        $('.sideScreenNav').addClass('_first');
    },
    removeSidebarColor: function (i) {
        $('.sideScreenNav').removeClass('_first');
    },
    showScheme: function (i) {
        var scale = 0.8;
        if (i==1) {
            scale = 1;
        }
        var screen = $('.schemeWrp');
        schemeTimer = setTimeout(function () {
            screen.css({
                transition: 'all .7s',
                opacity: 1,
                visibility: 'visible',
                transform: 'translateY(0) scale('+scale+')'
            });
        }, 700);
    },
    hideScheme: function () {
        clearTimeout(schemeTimer);
        var screen = $('.schemeWrp');
        screen.css({
            transition: 'all .2s',
            transform: 'translateY(-10%)',
            visibility: 'hidden',
            opacity: 0
        });
    },
    changeScheme: function(i) {
        var item = $('.circle');
        item.removeClass('_act');
        item.eq(i-2).addClass('_act');
    }
};